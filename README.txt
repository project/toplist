
Toplist Module
--------------
by Marco Olivo, me@olivo.net - www.olivo.net



Description
-----------
With this module you can run a section on your Drupal site that features
top referreral sites. Ever wished to have a top sites list on your website?
With this module you can.


Installation
------------
Simple copy the files in your Drupal modules directory and activate the module.


How to use it
-------------
Go to 'admin/content/toplist' to manually manage approved sites that will appear
in your top sites list.
Users can add their by filling in the form at the 'add-your-site' URL. You will find
also a list of all the approved sites at 'toplist'.
